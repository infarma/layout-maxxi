package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RetornoDoPedido struct {
	Identificador                      string `json:"Identificador"`
	CnpjFilialMaxxi                    int64  `json:"CnpjFilialMaxxi"`
	NumeroPedidoMaxxi                  int32  `json:"NumeroPedidoMaxxi"`
	NumeroPedidoFaturadoPeloFornecedor int32  `json:"NumeroPedidoFaturadoPeloFornecedor"`
	AtendidoOuNao                      int32  `json:"AtendidoOuNao"`
}

func (r *RetornoDoPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRetornoDoPedido

	err = posicaoParaValor.ReturnByType(&r.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CnpjFilialMaxxi, "CnpjFilialMaxxi")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroPedidoMaxxi, "NumeroPedidoMaxxi")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroPedidoFaturadoPeloFornecedor, "NumeroPedidoFaturadoPeloFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.AtendidoOuNao, "AtendidoOuNao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRetornoDoPedido = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":                      {0, 3, 0},
	"CnpjFilialMaxxi":                    {3, 17, 0},
	"NumeroPedidoMaxxi":                  {17, 25, 0},
	"NumeroPedidoFaturadoPeloFornecedor": {25, 33, 0},
	"AtendidoOuNao":                      {33, 35, 0},
}
