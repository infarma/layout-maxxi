package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type ItensNaoAtendidos struct {
	Identificador         string `json:"Identificador"`
	EanProduto            int64  `json:"EanProduto"`
	QuantidadeNaoAtendida int32  `json:"QuantidadeNaoAtendida"`
	SemUso                string `json:"SemUso"`
	MotivoNaoAtendimento  int32  `json:"MotivoNaoAtendimento"`
}

func (i *ItensNaoAtendidos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItensNaoAtendidos

	err = posicaoParaValor.ReturnByType(&i.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.EanProduto, "EanProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.QuantidadeNaoAtendida, "QuantidadeNaoAtendida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.SemUso, "SemUso")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.MotivoNaoAtendimento, "MotivoNaoAtendimento")
	if err != nil {
		return err
	}

	return err
}

var PosicoesItensNaoAtendidos = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":         {0, 3, 0},
	"EanProduto":            {3, 16, 0},
	"QuantidadeNaoAtendida": {16, 20, 0},
	"SemUso":                {20, 34, 0},
	"MotivoNaoAtendimento":  {34, 35, 0},
}
