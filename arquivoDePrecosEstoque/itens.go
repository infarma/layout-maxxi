package arquivoDePrecosEstoque

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Itens struct {
	Identificador           string  `json:"Identificador"`
	Ean                     string  `json:"Ean"`
	CodigoInternoFornecedor int64   `json:"CodigoInternoFornecedor"`
	PrecoVenda              float64 `json:"PrecoVenda"`
	Estoque                 int32   `json:"Estoque"`
	PrecoCondicao           float64 `json:"PrecoCondicao"`
	FinalRegistro           string  `json:"FinalRegistro"`
}

func (i *Itens) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItens

	err = posicaoParaValor.ReturnByType(&i.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Ean, "Ean")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoInternoFornecedor, "CodigoInternoFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.PrecoVenda, "PrecoVenda")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Estoque, "Estoque")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.PrecoCondicao, "PrecoCondicao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.FinalRegistro, "FinalRegistro")
	if err != nil {
		return err
	}

	return err
}

var PosicoesItens = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":           {0, 3, 0},
	"Ean":                     {3, 16, 0},
	"CodigoInternoFornecedor": {16, 26, 0},
	"PrecoVenda":              {26, 41, 2},
	"Estoque":                 {41, 47, 0},
	"PrecoCondicao":           {47, 62, 2},
	"FinalRegistro":           {17, 67, 0},
}
