package arquivoDePrecosEstoque

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Finaliacao struct {
	Identificador string `json:"Identificador"`
	TotalItens    int32  `json:"TotalItens"`
	FinalRegistro string `json:"FinalRegistro"`
}

func (f *Finaliacao) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesFinaliacao

	err = posicaoParaValor.ReturnByType(&f.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&f.TotalItens, "TotalItens")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&f.FinalRegistro, "FinalRegistro")
	if err != nil {
		return err
	}

	return err
}

var PosicoesFinaliacao = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador": {0, 3, 0},
	"TotalItens":    {3, 9, 0},
	"FinalRegistro": {9, 67, 0},
}
