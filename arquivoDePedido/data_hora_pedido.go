package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type DataHoraPedido struct {
	Indentificador string `json:"Indentificador"`
	DataPedido     int32  `json:"DataPedido"`
	HoraPedido     int32  `json:"HoraPedido"`
	FinalRegistro  string `json:"FinalRegistro"`
}

func (d *DataHoraPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDataHoraPedido

	err = posicaoParaValor.ReturnByType(&d.Indentificador, "Indentificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.DataPedido, "DataPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.HoraPedido, "HoraPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.FinalRegistro, "FinalRegistro")
	if err != nil {
		return err
	}

	return err
}

var PosicoesDataHoraPedido = map[string]gerador_layouts_posicoes.Posicao{
	"Indentificador": {0, 3, 0},
	"DataPedido":     {3, 11, 0},
	"HoraPedido":     {11, 17, 0},
	"FinalRegistro":  {17, 45, 0},
}
