package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type NumeroPedido struct {
	Identificador     string `json:"Identificador"`
	SemUso            int64  `json:"SemUso"`
	CnpjFornecedor    int64  `json:"CnpjFornecedor"`
	NumeroPedidoMaxxi int32  `json:"NumeroPedidoMaxxi"`
	FinalRegistro     string `json:"FinalRegistro"`
}

func (n *NumeroPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesNumeroPedido

	err = posicaoParaValor.ReturnByType(&n.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&n.SemUso, "SemUso")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&n.CnpjFornecedor, "CnpjFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&n.NumeroPedidoMaxxi, "NumeroPedidoMaxxi")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&n.FinalRegistro, "FinalRegistro")
	if err != nil {
		return err
	}

	return err
}

var PosicoesNumeroPedido = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":     {0, 3, 0},
	"SemUso":            {3, 11, 0},
	"CnpjFornecedor":    {11, 25, 0},
	"NumeroPedidoMaxxi": {25, 33, 0},
	"FinalRegistro":     {33, 45, 0},
}
