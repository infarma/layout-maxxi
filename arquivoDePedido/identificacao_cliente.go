package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type IdentificacaoCliente struct {
	Identificador   string `json:"Identificador"`
	CnpjFilialMaxxi int64  `json:"CnpjFilialMaxxi"`
	TipoFaturamento int32  `json:"TipoFaturamento"`
	FinalRegistro   string `json:"FinalRegistro"`
}

func (i *IdentificacaoCliente) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesIdentificacaoCliente

	err = posicaoParaValor.ReturnByType(&i.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CnpjFilialMaxxi, "CnpjFilialMaxxi")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.TipoFaturamento, "TipoFaturamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.FinalRegistro, "FinalRegistro")
	if err != nil {
		return err
	}

	return err
}

var PosicoesIdentificacaoCliente = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":   {0, 3, 0},
	"CnpjFilialMaxxi": {3, 17, 0},
	"TipoFaturamento": {17, 18, 0},
	"FinalRegistro":   {18, 45, 0},
}
