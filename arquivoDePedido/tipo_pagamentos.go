package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type TipoPagamentos struct {
	Identificador string `json:"Identificador"`
	TipoPagamento int32  `json:"TipoPagamento"`
	FinalRegistro string `json:"FinalRegistro"`
}

func (t *TipoPagamentos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTipoPagamentos

	err = posicaoParaValor.ReturnByType(&t.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.TipoPagamento, "TipoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.FinalRegistro, "FinalRegistro")
	if err != nil {
		return err
	}

	return err
}

var PosicoesTipoPagamentos = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador": {0, 3, 0},
	"TipoPagamento": {3, 4, 0},
	"FinalRegistro": {4, 45, 0},
}
