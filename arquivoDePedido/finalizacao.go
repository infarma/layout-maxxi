package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Finalizacao struct {
	Identificador    string `json:"Identificador"`
	QuantidadesItens int32  `json:"QuantidadesItens"`
	FinalRegistro    string `json:"FinalRegistro"`
}

func (f *Finalizacao) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesFinalizacao

	err = posicaoParaValor.ReturnByType(&f.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&f.QuantidadesItens, "QuantidadesItens")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&f.FinalRegistro, "FinalRegistro")
	if err != nil {
		return err
	}

	return err
}

var PosicoesFinalizacao = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":    {0, 3, 0},
	"QuantidadesItens": {3, 9, 0},
	"FinalRegistro":    {9, 45, 0},
}
