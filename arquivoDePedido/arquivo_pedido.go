package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	IdentificacaoCliente IdentificacaoCliente `json:"IdentificacaoCliente"`
	NumeroPedido         NumeroPedido         `json:"NumeroPedido"`
	TipoPagamentos       TipoPagamentos       `json:"TipoPagamentos"`
	DataHoraPedido       DataHoraPedido       `json:"DataHoraPedido"`
	ItensPedido          []ItensPedido        `json:"ItensPedido"`
	Finalizacao          Finalizacao          `json:"Finalizacao"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:3])

		var index int32
		if identificador == "001" {
			err = arquivo.IdentificacaoCliente.ComposeStruct(string(runes))
		} else if identificador == "002" {
			err = arquivo.NumeroPedido.ComposeStruct(string(runes))
		} else if identificador == "003" {
			err = arquivo.TipoPagamentos.ComposeStruct(string(runes))
		} else if identificador == "004" {
			err = arquivo.DataHoraPedido.ComposeStruct(string(runes))
		} else if identificador == "005" {
			err = arquivo.ItensPedido[index].ComposeStruct(string(runes))
			index++
		} else if identificador == "006" {
			err = arquivo.Finalizacao.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}
