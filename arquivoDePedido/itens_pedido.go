package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type ItensPedido struct {
	Identificador           string  `json:"Identificador"`
	CodigoEanProduto        string  `json:"CodigoEanProduto"`
	CodigoInternoFornecedor int64   `json:"CodigoInternoFornecedor"`
	QuantidadePedida        int32   `json:"QuantidadePedida"`
	PrecoCondicao           float64 `json:"PrecoCondicao"`
}

func (i *ItensPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItensPedido

	err = posicaoParaValor.ReturnByType(&i.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoEanProduto, "CodigoEanProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoInternoFornecedor, "CodigoInternoFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.QuantidadePedida, "QuantidadePedida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.PrecoCondicao, "PrecoCondicao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesItensPedido = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":           {0, 3, 0},
	"CodigoEanProduto":        {3, 16, 0},
	"CodigoInternoFornecedor": {16, 26, 0},
	"QuantidadePedida":        {26, 30, 0},
	"PrecoCondicao":           {30, 45, 2},
}
