package arquivoNotasFiscais

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type NotaFiscalEmitida struct {
	CnpjFilialMaxxi  int64   `json:"CnpjFilialMaxxi"`
	NumeroNotaFiscal int64   `json:"NumeroNotaFiscal"`
	DataEmissao      int32   `json:"DataEmissao"`
	NumeroFatura     int64   `json:"NumeroFatura"`
	ValorTotalNota   float64 `json:"ValorTotalNota"`
	DataVencimento   int32   `json:"DataVencimento"`
	ValorVencimento  float64 `json:"ValorVencimento"`
}

func (n *NotaFiscalEmitida) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesNotaFiscalEmitida

	err = posicaoParaValor.ReturnByType(&n.CnpjFilialMaxxi, "CnpjFilialMaxxi")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&n.NumeroNotaFiscal, "NumeroNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&n.DataEmissao, "DataEmissao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&n.NumeroFatura, "NumeroFatura")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&n.ValorTotalNota, "ValorTotalNota")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&n.DataVencimento, "DataVencimento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&n.ValorVencimento, "ValorVencimento")
	if err != nil {
		return err
	}

	return err
}

var PosicoesNotaFiscalEmitida = map[string]gerador_layouts_posicoes.Posicao{
	"CnpjFilialMaxxi":  {0, 14, 0},
	"NumeroNotaFiscal": {14, 24, 0},
	"DataEmissao":      {24, 32, 0},
	"NumeroFatura":     {32, 44, 0},
	"ValorTotalNota":   {44, 59, 2},
	"DataVencimento":   {59, 67, 0},
	"ValorVencimento":  {67, 82, 2},
}
