## Arquivo de Precos e Estoque
gerador-layouts arquivoDePrecosEstoque cabecalho Identificador:string:0:3 DataGeracao:int32:3:11 HoraGeracao:int32:11:17 FinalRegistro:string:17:67

gerador-layouts arquivoDePrecosEstoque itens Identificador:string:0:3 Ean:string:3:16 CodigoInternoFornecedor:int64:16:26 PrecoVenda:float64:26:41:2 Estoque:int32:41:47 PrecoCondicao:float64:47:62:2 FinalRegistro:string:17:67

gerador-layouts arquivoDePrecosEstoque finaliacao Identificador:string:0:3 TotalItens:int32:3:9 FinalRegistro:string:9:67


## Arquivo de Pedido
gerador-layouts arquivoDePedido IdentificacaoCliente Identificador:string:0:3 CnpjFilialMaxxi:int64:3:17 TipoFaturamento:int32:17:18 FinalRegistro:string:18:45

gerador-layouts arquivoDePedido NumeroPedido Identificador:string:0:3 SemUso:int64:3:11 CnpjFornecedor:int64:11:25 NumeroPedidoMaxxi:int32:25:33 FinalRegistro:string:33:45
 
gerador-layouts arquivoDePedido TipoPagamentos Identificador:string:0:3 TipoPagamento:int32:3:4 FinalRegistro:string:4:45

gerador-layouts arquivoDePedido DataHoraPedido Indentificador:string:0:3 DataPedido:int32:3:11 HoraPedido:int32:11:17 FinalRegistro:string:17:45

gerador-layouts arquivoDePedido itensPedido Identificador:string:0:3 CodigoEanProduto:string:3:16 CodigoInternoFornecedor:int64:16:26 QuantidadePedida:int32:26:30 PrecoCondicao:float64:30:45:2

gerador-layouts arquivoDePedido finalizacao Identificador:string:0:3 QuantidadesItens:int32:3:9 FinalRegistro:string:9:45


## Retorno de Pedido
gerador-layouts retornoDePedido RetornoDoPedido Identificador:string:0:3 CnpjFilialMaxxi:int64:3:17 NumeroPedidoMaxxi:int32:17:25 NumeroPedidoFaturadoPeloFornecedor:int32:25:33 AtendidoOuNao:int32:33:35

gerador-layouts retornoDePedido ItensNaoAtendidos Identificador:string:0:3 EanProduto:int64:3:16 QuantidadeNaoAtendida:int32:16:20 SemUso:string:20:34 MotivoNaoAtendimento:int32:34:35


## Arquivo de Notas Fiscais
gerador-layouts arquivoNotasFiscais NotaFiscalEmitida CnpjFilialMaxxi:int64:0:14 NumeroNotaFiscal:int64:14:24 DataEmissao:int32:24:32 NumeroFatura:int64:32:44 ValorTotalNota:float64:44:59:2 DataVencimento:int32:59:67 ValorVencimento:float64:67:82:2


## Arquivo de Pagamentos
gerador-layouts arquivoDePagamentos cabecalho Identificador:string:0:3 DataGeracao:int32:3:11 HoraGeracao:int32:11:17 CnpjFornecedor:int64:17:31 CnpjMaxxi:int64:31:45 FinalRegistro:string:45:90

gerador-layouts arquivoDePagamentos detalhesPagamento Identificador:string:0:3 NumeroNotaFiscal:int64:3:13 DataPagamento:int32:13:21 NumeroDuplicata:int64:21:31 ValorDuplicata:float64:31:46:2 ValorDesconto:float64:46:61:2 ValorPagamento:float64:61:76:2 FinalRegistro:string:76:90

gerador-layouts arquivoDePagamentos finalizacao Identificador:string:0:3 TotalPagamentos:int32:3:9 ValorTotalDescontos:float64:9:24:2 ValorTotalPagamentos:float64:24:39:2 FinalRegistro:string:39:90

