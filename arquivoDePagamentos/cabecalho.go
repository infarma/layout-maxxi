package arquivoDePagamentos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Cabecalho struct {
	Identificador  string `json:"Identificador"`
	DataGeracao    int32  `json:"DataGeracao"`
	HoraGeracao    int32  `json:"HoraGeracao"`
	CnpjFornecedor int64  `json:"CnpjFornecedor"`
	CnpjMaxxi      int64  `json:"CnpjMaxxi"`
	FinalRegistro  string `json:"FinalRegistro"`
}

func (c *Cabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalho

	err = posicaoParaValor.ReturnByType(&c.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataGeracao, "DataGeracao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.HoraGeracao, "HoraGeracao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjFornecedor, "CnpjFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjMaxxi, "CnpjMaxxi")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.FinalRegistro, "FinalRegistro")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":  {0, 3, 0},
	"DataGeracao":    {3, 11, 0},
	"HoraGeracao":    {11, 17, 0},
	"CnpjFornecedor": {17, 31, 0},
	"CnpjMaxxi":      {31, 45, 0},
	"FinalRegistro":  {45, 90, 0},
}
