package arquivoDePagamentos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type DetalhesPagamento struct {
	Identificador    string  `json:"Identificador"`
	NumeroNotaFiscal int64   `json:"NumeroNotaFiscal"`
	DataPagamento    int32   `json:"DataPagamento"`
	NumeroDuplicata  int64   `json:"NumeroDuplicata"`
	ValorDuplicata   float64 `json:"ValorDuplicata"`
	ValorDesconto    float64 `json:"ValorDesconto"`
	ValorPagamento   float64 `json:"ValorPagamento"`
	FinalRegistro    string  `json:"FinalRegistro"`
}

func (d *DetalhesPagamento) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDetalhesPagamento

	err = posicaoParaValor.ReturnByType(&d.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.NumeroNotaFiscal, "NumeroNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.DataPagamento, "DataPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.NumeroDuplicata, "NumeroDuplicata")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDuplicata, "ValorDuplicata")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDesconto, "ValorDesconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorPagamento, "ValorPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.FinalRegistro, "FinalRegistro")
	if err != nil {
		return err
	}

	return err
}

var PosicoesDetalhesPagamento = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":    {0, 3, 0},
	"NumeroNotaFiscal": {3, 13, 0},
	"DataPagamento":    {13, 21, 0},
	"NumeroDuplicata":  {21, 31, 0},
	"ValorDuplicata":   {31, 46, 2},
	"ValorDesconto":    {46, 61, 2},
	"ValorPagamento":   {61, 76, 2},
	"FinalRegistro":    {76, 90, 0},
}
