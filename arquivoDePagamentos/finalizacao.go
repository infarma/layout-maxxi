package arquivoDePagamentos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Finalizacao struct {
	Identificador        string  `json:"Identificador"`
	TotalPagamentos      int32   `json:"TotalPagamentos"`
	ValorTotalDescontos  float64 `json:"ValorTotalDescontos"`
	ValorTotalPagamentos float64 `json:"ValorTotalPagamentos"`
	FinalRegistro        string  `json:"FinalRegistro"`
}

func (f *Finalizacao) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesFinalizacao

	err = posicaoParaValor.ReturnByType(&f.Identificador, "Identificador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&f.TotalPagamentos, "TotalPagamentos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&f.ValorTotalDescontos, "ValorTotalDescontos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&f.ValorTotalPagamentos, "ValorTotalPagamentos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&f.FinalRegistro, "FinalRegistro")
	if err != nil {
		return err
	}

	return err
}

var PosicoesFinalizacao = map[string]gerador_layouts_posicoes.Posicao{
	"Identificador":        {0, 3, 0},
	"TotalPagamentos":      {3, 9, 0},
	"ValorTotalDescontos":  {9, 24, 2},
	"ValorTotalPagamentos": {24, 39, 2},
	"FinalRegistro":        {39, 90, 0},
}
